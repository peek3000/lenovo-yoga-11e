What Works Out Of The Box:
- Keyboard
- Mouse
- Touchscreen
- Touchscreen scrolling
    Note: Control Center > Mouse Preferences > Touchpad
    Select: Two-finger scrolling
    Select: Enable horizontal scrolling
- Touchscreen right-click
- Audio
- Microphone
- Mute, Volume Up, Volume Down

What Does Not Work Out Of The Box:
- Microphone Mute
- Brightness Up, Brightness Down
- Wifi on/off

My Modifications:
- Control Center > Keyboard Shortcuts > Raise window if it's covered by another window, otherwise lower it = Mod4+`
- Control Center > Onboard > General > Start Onboard hidden
- Control Center > Onboard > General > Show floating icon when Onboard is hidden
- Control Center > Windows > Behavior > Window Selection = Select windows when the mouse moves over them
- Control Center > Windows > Behavior > Movement Key = Super
- Control Center > Windows > General > Titlebar Buttons = Left
- MATE Tweak > Interface > Enable indicators
- MATE Tweak > Interface > Enable advanced menu
- MATE Tweak > Windows > Window manager = Marco (No compositor)
- Edit /etc/default/tlp
- MATE Tweak > Enable keyboard LEDdd
    # TLP_DEFAULT_MODE=AC
    TLP_DEFAULT_MODE=BAT
- NO: apt-get install iio-sensor-proxy [NOTE: DOES NOT SUPPORT Yoga 11e]
- NO: Edit /etc/default/grub
  GRUB_CMDLINE_LINUX_DEFAULT="quiet splash video.use_native_backlight=1"
  [NOTE: Does not work]
- NO: Edit /etc/default/grub
  GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"
  [NOTE: Does not work]
- NO: Edit /usr/share/X11/xorg.conf.d/20-intel.conf
	Section "Device"
		Identifier "card0"
		Driver     "intel"
		Option     "Backlight" "intel_backlight"
		BusID      "PCI:0:2:0"
	EndSection
  [NOTE: Does not work]
- NO: Edit /etc/default/grub
  GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=none"
  [NOTE: Does not work]
- NO: Edit /etc/default/grub
  GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_osi=Linux"
  [NOTE: Does not work]
- NO: Edit /etc/default/grub
  GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_osi="
  [NOTE: Does not work]
- Control Center > Keyboard Shortcuts > Windows Management > Move between windows, using a popup window = Mod4+Tab

Problems:
- matchbox-keyboard is too small
- matchbox-keyboard has no window decorations
- Backlighting can be controlled by /sys/class/backlight/intel_backlight/*

